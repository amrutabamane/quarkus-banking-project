package org.example.controller;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.example.dto.JwtAuthRequestDto;
import org.example.dto.UserDto;
import org.example.entity.UserEntity;
import org.example.exception.UserNotFoundException;
import org.example.repository.UserRepository;
import org.example.service.UserService;

@Path("/users")

@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT"

)
public class UserController {

    @Inject
    UserRepository userRepository;

    @Inject
    UserService userService;



    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @RolesAllowed({"admin", "normal"})
    public Response registerUser(@Valid UserDto userDto) {

        UserEntity registeredUser = this.userService.registerNewUser(userDto);
        return Response.ok(registeredUser).build();

    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response loginUser(JwtAuthRequestDto jwtAuthRequestDto) throws UserNotFoundException {

        //return Response.ok(userService.loginUser(jwtAuthRequestDto)).build();
        try{
            //int out = 8 / i;
            return Response.ok(userService.loginUser(jwtAuthRequestDto)).build();
        }catch (Exception e){
            throw new UserNotFoundException("user not found with name: "+jwtAuthRequestDto.getName());

        }

    }

    @GET
    @Path("/getUserList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserList(){
        return Response.ok(userService.getUserList()).build();
    }


}
